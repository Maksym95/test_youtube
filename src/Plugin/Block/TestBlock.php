<?php

namespace Drupal\test_youtube\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Test' Block.
 *
 * @Block(
 *   id = "test_youtube",
 *   admin_label = @Translation("Test Youtube"),
 *   category = @Translation("Test Youtube"),
 * )
 */
class TestBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    if (!empty($config['test_youtube_url'])) {

      $url = $config['test_youtube_url'];
      $embed=substr(strstr($url, 'v='),2,11);

      return [
        '#markup' => t('<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$embed.'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
      ];
    }else {
      return [
        '#markup' => t('no video url')
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['test_youtube_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Youtube URL'),
      '#description' => $this->t('Please enter the link to the video from youtube'),
      '#default_value' => isset($config['test_youtube_url']) ? $config['test_youtube_url'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['test_youtube_url'] = $values['test_youtube_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $regex_pattern = '~
         ^(?:https?://)?                           # Optional protocol
          (?:www[.])?                              # Optional sub-domain
          (?:youtube[.]com/watch[?]v=|youtu[.]be/) # Mandatory domain name (w/ query string in .com)
          ([^&]{11})                               # Video id of 11 characters as capture group 1
           ~x';
    $match;
    $url=$form_state->getValue('test_youtube_url');
    if($url<>'') {  //if not empty
      if (!preg_match($regex_pattern, $url, $match)) {
        $form_state->setErrorByName('test_youtube_url', $this->t('It is not youtube url'));
      }
    }
  }

}
